import {
    ADD_CALL_UP,
    ADD_CALL_DOWN,
    MOVE_DOWN,
    MOVE_UP,
    ADD_DESTINATION,
    CLEAR_CALL_UP,
    CLEAR_CALL_DOWN,
    CLEAR_DESTINATION,
    SET_NO_QUEUE,
    CHANGE_DIRECTION,
    HEARTBEAT,
    RESET_DIRECTION,
  } from './types';
  
  
  export const addCall = (number, direction) => dispatch => {
    return new Promise((resolve, reject) => {
      if(direction === 'up'){
        dispatch({
          type: ADD_CALL_UP,
          payload: number,
        })
      }else{
        dispatch({
          type: ADD_CALL_DOWN,
          payload: number
        })
      }
      resolve()
    })
  };

  export const addDestination = (number) => dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: ADD_DESTINATION,
        payload: number,
      })

      resolve()
    })
    
  };

  export const moveUp = () => dispatch => {
    dispatch({
      type: MOVE_UP,
    });
  };

  export const moveDown = () => dispatch => {
    dispatch({
      type: MOVE_DOWN,
    });
  };

  export const clearDestination = (floor) => dispatch => {
    dispatch({
      type: CLEAR_DESTINATION,
      payload: floor,
    });
  };

  export const clearCallUp = (floor) => dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: CLEAR_CALL_UP,
        payload: floor,
      })
      resolve()
    })
    
  };

  export const clearCallDown = (floor) => dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: CLEAR_CALL_DOWN,
        payload: floor,
      });
      resolve()
    })
    
  };

  export const setNoQueue = () => dispatch => {
    dispatch({
      type: SET_NO_QUEUE,
    })
  }

  export const changeDirection = () => dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: CHANGE_DIRECTION,
      })

      resolve();
    })
  }

  export const doHeartbeat = () => dispatch => {
    dispatch({
      type: HEARTBEAT,
    })
  }

  export const resetElevator = () => dispatch => {
    dispatch({
      type: RESET_DIRECTION,
    })
  }