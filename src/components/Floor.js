import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addCall, doHeartbeat } from '../actions/elevatorActions';
import floors from '../reducers/floors';
//import M from 'materialize-css/dist/js/materialize.min.js';

const Floor = ({ floor, elevator: {current_floor, call_ups, call_downs}, addCall, doHeartbeat }) => {
  
  const addCallClick = (direction) => {
    addCall(floor.number, direction).then(() => {
      doHeartbeat()
    })
  }

  return (
    // generate list item for each log item
    // <li className="collection-item">
    <li className={`collection-item ${
        current_floor === floor.number ? 'active-floor' : ''
      }`}>
      <div>
        <span className="mr-5">{ floor.number }</span>
        {
            floor.number !== floors.length ? 
            <a onClick={() => addCallClick('up')} className={`btn panel-btn mr-5 ${call_ups.includes(floor.number) ? 'panel-selected' : ''}`}>
              <i className="material-icons">arrow_upward</i>
            </a> : ''
        }
        
        {
            floor.number !== 1 ?
            <a onClick={() => addCallClick('down')} className={`btn panel-btn ${call_downs.includes(floor.number) ? 'panel-selected' : ''}`}>
              <i className="material-icons">arrow_downward</i>
            </a> : ''
        }
      </div>
    </li>
  );
};

Floor.propTypes = {
  floor: PropTypes.object.isRequired,
};

//Access the state variables as props
const mapStateToProps = state => ({
    elevator: state.elevator
  });

export default connect(mapStateToProps, {addCall, doHeartbeat})(Floor);
