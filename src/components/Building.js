import React, { useEffect, Fragment, useState } from 'react';
import { connect } from 'react-redux';
import Floor from './Floor';
import { moveDown, moveUp, clearDestination, clearCallUp, clearCallDown, changeDirection, resetElevator } from '../actions/elevatorActions';

export const Building = ({ 
    elevator: { 
        floors,
        call_ups, 
        call_downs, 
        destinations, 
        current_floor, 
        direction,
        heartbeat,
    }, 
        moveDown, 
        moveUp, 
        clearDestination, 
        clearCallUp, 
        clearCallDown,
        changeDirection,
        resetElevator,
    }) => {

  const [processing, setProcessing] = useState(false)
  // Runs if floor changes, or if heartbeat(A destination or call button was pressed)
  useEffect(() => {
    
    // Check if function already running
    if(processing){
      return
    }
    setProcessing(true)
    
    
    setTimeout(function(){
      
      // if no instructions and floor is not 1
      if((destinations.length === 0 && call_ups.length === 0 && call_downs.length === 0) && current_floor > 1)
      {
        console.log('returning home')
        if(direction === 'up'){
          changeDirection().then(() => {
            setProcessing(false)
            moveDown()
            return
          })
        } else {
          setProcessing(false)
          moveDown()
          return
        }
      }
      // if no instructions and ground floor
      if((destinations.length === 0 && call_ups.length === 0 && call_downs.length === 0) && current_floor === 1)
      {
        console.log('home')
        setProcessing(false)
        resetElevator()
        return
      }

      // Get current state of queues
      let higher_destinations = destinations.filter(destination => destination > current_floor)
      let higher_call_ups = call_ups.filter(call_up => call_up > current_floor)
      let higher_call_downs = call_downs.filter(call_down => call_down > current_floor)

      let lower_destinations = destinations.filter(destination => destination < current_floor)
      let lower_call_ups = call_ups.filter(call_up => call_up < current_floor)
      let lower_call_downs = call_downs.filter(call_down => call_down < current_floor)

      let is_destination = destinations.includes(current_floor)
      let is_call_up = call_ups.includes(current_floor)
      let is_call_down = call_downs.includes(current_floor)


      // GOING UP
      if(direction === 'up')
      {
        // I do have further to go but i dont need to stop
        if(higher_destinations.length > 0 && !is_call_up && !is_destination){
          console.log('no stop, moving up')
          setProcessing(false)
            moveUp()
            return
        }

        // I do have further to go and i need to stop
        if(( is_destination || is_call_up ) && higher_destinations.length > 0){
          console.log('stopped, but will continue')
          setTimeout(function() {
            clearDestination(current_floor)
            clearCallUp(current_floor).then(() => {
              setProcessing(false)
              moveUp()
            })
          }, 1000)
          return
        }
        // I don't have further destinations but someone is calling me in the same direction, but to go back the other way
        if( higher_destinations.length === 0 && higher_call_downs.length > 0){
          if(is_destination){
            console.log('Stopped for destination, then keep going to opposite call')
            setTimeout(() => {
              clearDestination(current_floor)
              setProcessing(false)
              moveUp()
            }, 1000)
            return
          } else {
            console.log('Keep going to opposite call')
            setProcessing(false)
            moveUp()
            return
          }
        }

        // I dont have further to go but i need to stop, either destination or go back the other way
        if( is_destination || is_call_down && higher_destinations.length === 0){
          console.log('stopped final destination, or opposite call')
          setTimeout(function() {
            clearDestination(current_floor)
            clearCallDown(current_floor)
            changeDirection().then(() => {
              setProcessing(false)
              moveDown()
            })
          }, 1000)
          return
        }

        // A new call in same direction, while no other instructions
        if( destinations.length === 0 && call_downs.length === 0 && call_ups.length > 0){
          if(lower_call_ups.length > 0){
            console.log('I have a new same direction call but its behind me')
            changeDirection().then(() => {
              setProcessing(false)
              moveDown()
            })
          } else {
            console.log('I have a new same direction call')
            setProcessing(false)
            moveUp()
          }
          return
        }

        // Stop for same direction call when no other instructions
        if( is_call_up && destinations.length === 0 && call_downs.length === 0){
          console.log('Stop for same direction call')
          setTimeout(() => {
            clearCallUp(current_floor)
            setProcessing(false)
            moveUp()
          }, 1000)
          return
        }
        
        

      // GOING DOWN
      } else if (direction === 'down'){
        // I do have further to go but i dont need to stop
        if(lower_destinations.length > 0 && !is_call_down && !is_destination){
          console.log('no stop, moving up')
          setProcessing(false)
            moveDown()
            return
        }
        // I do have further to go and i need to stop
        if(( is_destination || is_call_down ) && lower_destinations.length > 0){
          console.log('stopped, but will continue')
          setTimeout(function() {
            clearDestination(current_floor)
            clearCallDown(current_floor).then(() => {
              setProcessing(false)
              moveDown()
            })
          }, 1000)
          return
        }
        // I don't have further destinations but someone is calling me in the same direction, but to go back the other way
        if( lower_destinations.length === 0 && lower_call_ups.length > 0){
          if(is_destination){
            console.log('Stopped for destination, then keep going to opposite call')
            setTimeout(() => {
              clearDestination(current_floor)
              setProcessing(false)
              moveDown()
            }, 1000)
            return
          } else {
            console.log('Keep going to opposite call')
            setProcessing(false)
            moveDown()
            return
          }
        }

        // I dont have further to go but i need to stop, either destination or go back the other way
        if( is_destination || is_call_up && lower_destinations.length === 0){
          console.log('stopped final destination, or opposite call')
          setTimeout(function() {
            clearDestination(current_floor)
            clearCallUp(current_floor)
            changeDirection().then(() => {
              setProcessing(false)
              moveUp()
            })
          }, 1000)
          return
        }
        
        // A new call in same direction, while no other instructions
        if( destinations.length === 0 && call_ups.length === 0 && call_downs.length > 0){
          if(higher_call_downs.length > 0){
            console.log('I have a new same direction call but its behind me')
            changeDirection().then(() => {
              setProcessing(false)
              moveUp()
            })
          } else {
            console.log('I have a new same direction call')
            setProcessing(false)
            moveDown()
          }
          return
        }

        // Stop for same direction call when no other instructions
        if( is_call_down && destinations.length === 0 && call_ups.length === 0){
          console.log('Stop for same direction call')
          setTimeout(() => {
            clearCallDown(current_floor)
            setProcessing(false)
            moveDown()
          }, 1000)
          return
        }

      }


    }, 1000)

    // eslint-disable-next-line
  }, [current_floor, heartbeat]);



  return (
    <Fragment>
    <ul className="collection with-header">
      <li className="collection-header">
        <h4 className="center">Building</h4>
      </li>
      {
        floors.map(floor => <Floor floor={floor} key={floor.number} />)
      }
    </ul>
    </Fragment>
  );
};

// Building.propTypes = {
//   floors: PropTypes.object.isRequired,
// };

//Access the state variables as props
const mapStateToProps = state => ({
  elevator: state.elevator
});

export default connect(mapStateToProps, {moveDown, moveUp, clearDestination, clearCallUp, clearCallDown, changeDirection, resetElevator})(Building);
