import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PanelButton from './PanelButton';

export const Panel = ({ elevator: { floors, destinations } }) => {
    
  return (
    <Fragment>
    <ul className="collection with-header">
      <li className="collection-header">
        <h4 className="center">Panel</h4>
      </li>
      <li>
          <div className="row pt-20">
          {
            floors.map(floor => <PanelButton floor={floor} key={floor.number} />)
          }
          </div>
      </li>
      
    </ul>
    </Fragment>
  );
};


//Access the state variables as props
const mapStateToProps = state => ({
  elevator: state.elevator
});

//connect 2nd param is object with actions to import
export default connect(mapStateToProps)(Panel);
