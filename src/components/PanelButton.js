import React from 'react';
import { connect } from 'react-redux';
import { addDestination, doHeartbeat } from '../actions/elevatorActions';
import PropTypes from 'prop-types';

const PanelButton = ({ floor, elevator: {destinations}, addDestination, doHeartbeat }) => {

  const panelClicked = () => {
    addDestination(floor.number).then(() => {
      doHeartbeat()
    })
  }

  return (
      <div className="col s6 center-align mb-5">
          <a onClick={() => panelClicked()} className={`btn panel-btn ${destinations.includes(floor.number) ? 'panel-selected' : ''}`}>
            { floor.number }
          </a>
      </div>
  );
};

PanelButton.propTypes = {
  floor: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    elevator: state.elevator
  });

export default connect(mapStateToProps, {addDestination, doHeartbeat})(PanelButton);
