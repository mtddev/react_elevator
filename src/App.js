import React, { useEffect, Fragment } from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import Building from './components/Building';
import Panel from './components/Panel';
import { Provider } from 'react-redux';
import store from './store';

import './App.css';

const App = () => {
  useEffect(() => {
    //Init Materialize JS
    M.AutoInit();
  });

  return (
    <Provider store={store}>
      <Fragment>
        <div className="container">
          <div className="row">
            <div className="col s8">
              <Building />
            </div>
            <div className="col s4">
              <Panel />
            </div>

          </div>
        </div>
      </Fragment>
    </Provider>
  );
};

export default App;
