import { combineReducers } from 'redux';
import elevatorReducer from './elevatorReducer';

//Object which holds all reducers
//nameofState: Reducer
export default combineReducers({
  elevator: elevatorReducer,
});
