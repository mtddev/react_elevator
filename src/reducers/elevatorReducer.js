import {
  SET_MOVING,
  ADD_DESTINATION,
  ADD_CALL_UP,
  ADD_CALL_DOWN,
  MOVE_UP,
  MOVE_DOWN,
  CLEAR_CALL_UP,
  CLEAR_CALL_DOWN,
  CLEAR_DESTINATION,
  SET_NO_QUEUE,
  CHANGE_DIRECTION,
  HEARTBEAT,
  RESET_DIRECTION,
} from '../actions/types';
import floors from './floors';

const initialState = {
  floors: floors,
  destinations: [],
  call_ups: [],
  call_downs: [],
  current_floor: 1,
  direction: 'up',
  queue: false,
  moving: false,
  heartbeat: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MOVING:
      return {
        ...state,
        moving: true
      };
    case ADD_DESTINATION:
      if(state.destinations.includes(action.payload))
      {
        return state;
      } else {
        return {
          ...state,
          destinations: [...state.destinations, action.payload],
          queue: true,
        };
      }

    case ADD_CALL_UP:
      if(state.call_ups.includes(action.payload)){
        return state;
      }else{
        return {
          ...state,
          call_ups: [...state.call_ups, action.payload],
          queue: true,
        };
      }
    
      case ADD_CALL_DOWN:
        if(state.call_downs.includes(action.payload)){
          return state;
        }else{
          return {
            ...state,
            call_downs: [...state.call_downs, action.payload],
            queue: true,
          };
        }

    case MOVE_DOWN:
        return {
          ...state,
          current_floor: state.current_floor - 1,
        };

    case MOVE_UP:
      return {
        ...state,
        current_floor: state.current_floor + 1,
      };

    case CLEAR_CALL_UP:
      return {
        ...state,
        call_ups: state.call_ups.filter(floor => floor !== action.payload),
      };
    
    case CLEAR_CALL_DOWN:
      return {
        ...state,
        call_downs: state.call_downs.filter(floor => floor !== action.payload),
      };

    case CLEAR_DESTINATION:
      return {
        ...state,
        destinations: state.destinations.filter(floor => floor !== action.payload),
      };

    case SET_NO_QUEUE:
      return {
        ...state,
        queue: false,
      };

    case CHANGE_DIRECTION:
      let new_direction = state.direction === 'up' ? 'down' : 'up'
      return {
        ...state,
        direction: new_direction,
      }

    case HEARTBEAT:
      return {
        ...state,
        heartbeat: !state.heartbeat
      }

    case RESET_DIRECTION:
      return {
        ...state,
        direction: 'up'
      }

    default:
      return state;
  }
};
